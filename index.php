<?php
/*
data sample
INSERT INTO `User` 
(`user_id`, `First_Name`, `Last_Name`, `Address1`, `City`, `Province`, `Country_Id`, `Postal_Code`, `Phone_Number`, `Cellphone_Number`, `Company`, `Job_Title`, `email`, 
`Registration_Date`, `Last_Modified_Date`, `Note`, `Active`, `Sugar_Id`, `Sugar_Account`, `Hash_Password`, `Last_Login_Date`, `Company_Domain`, `User_Level`, `Created_By`,
 `Avator`, `Newsletter`, `Token`, `invitedby`, `companyAvatar`, `dealer`, `Billing_Country_Id`, `Billing_Province`) 
 VALUES
(1,	'Frida',	'Kahlo',	'Calle Etna 46 int. 404 col.',	'Ciudad de Mexico',	'DF',	156,	'01010',	'6477023261',	NULL,	'Frida Kahlo',	NULL,	'fridakahlo.moacast@gmail.com',
    NULL,	'2020-03-03 18:29:58',	NULL,	1,	NULL,	NULL,	'$2y$10$lJQdPxFtqtaCygiAqezRyuxbMmQ9cTV40gBlyJxZNtFvK684Jb0yS',	NULL,	NULL,	0,	0,
    	NULL,	NULL,	NULL,	NULL,	NULL,	0,	156,	'DF'),
*/
    $arrFromProvince=[
    'AB'=>'1',
    'BC'=>'2',
    'MB'=>'3',
    'NB'=>'4',
    'NF'=>'5',
    'NT'=>'6',
    'NU'=>'7',
    'NS'=>'8',
    'ON'=>'9',
    'PE'=>'10',
    'QC'=>'11',
    'SK'=>'12',
    'YT'=>'13',
    'AK'=>'14',
    'AL'=>'15',
    'AZ'=>'16',
    'AR'=>'17',
    'CA'=>'18',
    'CO'=>'19',
    'CT'=>'20',
    'DE'=>'21',
    'DC'=>'22',
    'FL'=>'23',
    'GA'=>'24',
    'GU'=>'25',
    'HI'=>'26',
    'ID'=>'27',
    'IL'=>'28',
    'IN'=>'29',
    'IA'=>'30',
    'KS'=>'31',
    'KY'=>'32',
    'LA'=>'33',
    'ME'=>'34',
    'MH'=>'35',
    'MD'=>'36',
    'MA'=>'37',
    'MI'=>'38',
    'MN'=>'39',
    'MS'=>'40',
    'MO'=>'41',
    'MT'=>'42',
    'NE'=>'43',
    'NV'=>'44',
    'NH'=>'45',
    'NJ'=>'46',
    'NM'=>'47',
    'NY'=>'48',
    'NC'=>'49',
    'ND'=>'50',
    'OH'=>'51',
    'OK'=>'52',
    'OR'=>'53',
    'PW'=>'54',
    'PA'=>'55',
    'PR'=>'56',
    'RI'=>'57',
    'SC'=>'58',
    'SD'=>'59',
    'TN'=>'60',
    'TX'=>'61',
    'UT'=>'62',
    'VT'=>'63',
    'VI'=>'64',
    'VA'=>'65',
    'WA'=>'66',
    'WV'=>'67',
    'WI'=>'68',
    'WY'=>'69',];
    if(file_exists('./cypress-cast_cypress_fixtures_users.json'))
    {
        $content=file_get_contents(('./cypress-cast_cypress_fixtures_users.json'));
        $arr=json_decode(($content));
        //User_id,User_First_Name,User_Last_Name,Shipping_Address1,Shipping_City,Shipping_Province,Shipping_Country_ID,Shipping_Postal,Phone_Number,NULL,Company,NULL,User_Email,
        //NULL,	'2020-03-03 18:29:58',NULL,	1,NULL,	NULL,	'$2y$10$lJQdPxFtqtaCygiAqezRyuxbMmQ9cTV40gBlyJxZNtFvK684Jb0yS',	NULL,	NULL,	0,	0,
        //NULL,	NULL,	NULL,	NULL,	NULL,	0,	Shipping_Country_ID,	transferred from array
        $res='';
        foreach($arr as $obj)
        {
            $realId=$obj->User_id+1;
            if(ctype_digit($obj->Billing_Province))
            {
                $finalBillingProvince=$obj->Billing_Province;
            }
            else
            {
                $finalBillingProvince=$arrFromProvince[$obj->Billing_Province];
            }
            $res.="({$realId},	'{$obj->User_First_Name}',	'{$obj->User_Last_Name}',	'{$obj->Shipping_Address1}',	'{$obj->Shipping_City}',	'{$obj->Shipping_Province}',	{$obj->Shipping_Country_ID},'{$obj->Shipping_Postal}',	'{$obj->Phone_Number}',	NULL,	'{$obj->Company}',	NULL,	'{$obj->User_Email}',	NULL,	'2020-03-03 18:29:58',	NULL,	1,	NULL,	NULL,	'$2y$10\$lJQdPxFtqtaCygiAqezRyuxbMmQ9cTV40gBlyJxZNtFvK684Jb0yS',	NULL,	NULL,	0,	0,	NULL,	NULL,	NULL,	NULL,	NULL,	0,	{$obj->Shipping_Country_ID},	'{$finalBillingProvince}'),\n";
        }
        echo $res;
    }
    else
    {
        die('file does not exist');
    }